const Boom = require('@hapi/boom')
const mongoose = require('./mongooseConnection')
const fs = require('fs')

const db = mongoose.connection
db.on(`error`, console.error.bind(console, 'connection error'))
db.once(`open`, () => console.log('Connected'))

const {userSchema, imageSchema} = require('./mongooseSchema')

const User = mongoose.model('User', userSchema)
const Image = mongoose.model('Image', imageSchema)

const rootHandler = (request, h) => {
    return h.response('This is a root route').code(200)
}

const registerHandler = async (request, h) => {
    const {payload} = request
    return User.create(payload)
        .then(doc => {
            fs.mkdirSync(`./images/${payload.email}`,{recursive: true})
            return h.response(doc).code(201)    
        })
        .catch(error => {
            const errorMessage = error.toString()
            if (errorMessage.match(/ValidationError|CastError/)) {
                return Boom.boomify(error, {statusCode: 400})    
            } else if(errorMessage.match('E11000')) {
                return Boom.boomify(error, {statusCode: 409})
            } else {
                return Boom.boomify(error)
            }    
        })
}

const loginHandler = async (request, h) => {
    const {payload} = request
    return User.findOne(payload).lean()
        .then(doc => {
            if(!doc) {
                return Boom.notFound('No User Found') 
            }
            return h.response(doc).code(202) 
        })
        .catch(error => {
            const errorMessage = error.toString()
            if (errorMessage.match(/ValidationError|CastError/)) {
                return Boom.boomify(error, {statusCode: 400})    
            } else if (errorMessage.match('E11000')) {
                return Boom.boomify(error, {statusCode: 409})
            } else {
                return Boom.boomify(error)
            }
        })
}

const editProfileHandler = async (request, h) => {
    const {payload, params} = request
    return User.findOneAndUpdate(params, payload, {new: true}).lean()
        .then(doc => {
            if (!doc) {
                return Boom.notFound('No User Found')
            }
            return h.response(doc).code(202)    
        })
        .catch(error => {
            const errorMessage = error.toString()
            if (errorMessage.match(/ValidationError|CastError/)) {
                return Boom.boomify(error, {statusCode: 400})
            } else if (errorMessage.match('E11000')) {
                return Boom.boomify(error, {statusCode: 409})
            } else {
                return Boom.boomify(error)
            }    
        })
}

const uploadImageHandler = async (request, h) => {
    const {payload, params} = request
    return Image.create({
        name: payload.name,
        email: params.email,
        caption: payload.caption,
        isDp: payload.isDp
    })
        .then(doc => {
            return new Promise((resolve, reject) => {
                const rs = fs.createWriteStream(`./images/${params.email}/${payload.name}`);
                (payload.image).pipe(rs)
                    .on('error', async () => {
                        await Image.findOneAndDelete({
                            email: params.email,
                            name: payload.name
                        })
                        const error = Boom.notAcceptable('Wrong email for images directory or invalid image file name')
                        error.reformat()
                        error.output.payload.detail = {
                            email: params.email,
                            name: payload.name
                        }
                        reject(error)
                    })
                    .on('finish', async () => {
                        resolve(h.response({
                            error: '',
                            message: 'Accepted',
                            detail: {
                                email: params.email,
                                name: payload.name,
                                caption: payload.caption
                            }
                        }).code(201))
                    })
            })
        })
        .catch(error => {
            const errorMessage = error.toString()
            if (errorMessage.match(/ValidationError|CastError/)) {
                return Boom.boomify(error, {statusCode: 400})
            } else if (errorMessage.match('E11000')) {
                return Boom.boomify(error, {statusCode: 409})
            } else {
                return Boom.boomify(error)
            }
        })
}

const loadingListHandler = async (request, h) => {
    return User.find({},{
        email: 1,
        firstName: 1,
        lastName: 1,
        displayPicture: 1,
        uniqueHandle: 1
    }).lean()
        .then(doc => {
            return h.response(doc).code(200) 
        })
        .catch(error => {
            const errorMessage = error.toString()
            if (errorMessage.match(/ValidationError|CastError/)) {
                return Boom.boomify(error, {statusCode: 400})    
            } else if (errorMessage.match('E11000')) {
                return Boom.boomify(error, {statusCode: 409})
            } else {
                return Boom.boomify(error)
            }
        })
}

module.exports = {
    rootHandler,
    registerHandler,
    loginHandler,
    editProfileHandler,
    uploadImageHandler,
    loadingListHandler
}