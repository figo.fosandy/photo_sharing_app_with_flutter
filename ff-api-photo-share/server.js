'use strict'
require('dotenv').config()

const Hapi = require('@hapi/hapi')
const Joi = require('@hapi/joi')
const Inert = require('@hapi/inert')

const config = require('./config.json')[process.env.NODE_ENV.toLowerCase()]

const {
    rootHandler,
    registerHandler,
    loginHandler,
    editProfileHandler,
    uploadImageHandler,
    loadingListHandler
} = require('./handler')

const init = async () => {
    const server = Hapi.server({
        port: config.port||process.env.PORT,
        host: '0.0.0.0'
    })

    await server.register(Inert)

    server.route([
        {
            method: 'GET',
            path: '/',
            handler: rootHandler
        },
        {
            method: 'POST',
            path: '/user/register',
            handler: registerHandler,
            options: {
                validate: {
                    payload: {
                        email: Joi.string().email().required(),
                        password: Joi.string().min(6).required()
                    }
                }
            }
        },
        {
            method: 'POST',
            path: '/user/login',
            handler: loginHandler,
            options: {
                validate: {
                    payload: {
                        email: Joi.string().email(),
                        uniqueHandle: Joi.string()
                    }
                }
            }
        },
        {
            method: 'PUT',
            path: '/user/edit/{email}',
            handler: editProfileHandler,
            options: {
                validate: {
                    params:{
                        email: Joi.string().email()
                    },
                    payload: {
                        firstName: Joi.string().regex(/^[a-zA-Z\s]+$/),
                        lastName: Joi.string().regex(/^[a-zA-Z\s]+$/),
                        displayPicture: Joi.string().regex(/[\w-]+\.(jpeg|jpg|png|gif){1}$/),
                        uniqueHandle: Joi.string().alphanum()
                    }
                }
            }
        },
        {
            method: 'POST',
            path: '/user/image/{email}',
            handler: uploadImageHandler,
            options: {
                validate:{
                    payload:{
                        image:Joi.required(),
                        name: Joi.string().regex(/[\w-]+\.(jpeg|jpg|png|gif){1}$/).required(),
                        caption: Joi.string().trim().default('Edit Profile'),
                        isDp: Joi.boolean().default(false)
                    }
                },
                payload:{
                    maxBytes:209715200,
                    output:'stream',
                    parse:true
                }
            }
        },
        {
            method: 'GET',
            path: '/images/{file*}',
            handler: {
                directory: {
                    path: './images',
                    listing: true
                }
            }
        },
        {
            method: 'GET',
            path: '/user/list',
            handler: loadingListHandler
        }
    ])
    
    await server.start()

    process.on('unhandledRejection',(err)=>{
        console.log(err)
        process.exit(1)
    })

    return server
}

module.exports = {init}