const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, 'Email id is required'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'Password is required'],
    },
    firstName: {
        type: String,
        index: true
    },
    lastName: {
        type: String,
        index: true
    },
    displayPicture: String,
    uniqueHandle: {
        type: String,
        default: Date.now,
        unique: true
    },
    friends: {
        type: Array,
        default: []
    }
})

const imageSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Image's name is required"],
        unique: true
    },
    email: {
        type: String,
        required: [true, "Image's owner is required"],
    },
    caption: {
        type: String,
        required: [true, "Image's caption is required"]
    },
    uploadedAt: {
        type: Number,
        default: Date.now
    },
    isDp: Boolean
})

module.exports = {userSchema, imageSchema}