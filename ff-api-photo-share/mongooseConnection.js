require('dotenv').config()

const mongoose=require('mongoose')
const config=require('./config.json')[process.env.NODE_ENV.toLowerCase()]

mongoose.connect(config.mongoServer,{
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useCreateIndex: true,
    useFindAndModify: false
})
mongoose.set('debug', true)

module.exports=mongoose