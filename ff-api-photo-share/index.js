const {init}=require('./server')

init()
    .then((server)=>{
        console.log(`Server running on ${server.info.uri}`)
    })