import 'package:flutter/material.dart';

import 'package:photo_sharing_app/src/app.dart';
import 'package:photo_sharing_app/src/models/app_state.dart';
import 'package:photo_sharing_app/src/redux/actions/load_user_list.dart';
import 'package:photo_sharing_app/src/redux/reducers/reducers.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';
import 'package:redux_thunk/redux_thunk.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  
  final persistor = Persistor<AppState>(
    storage: FlutterStorage(),
    serializer: JsonSerializer<AppState>(AppState.fromJSON)
  );

  final initialState = await persistor.load();

  final store = Store<AppState>(
    appStateReducer,
    initialState: initialState,
    middleware: [persistor.createMiddleware(), thunkMiddleware, new LoggingMiddleware.printer()]
  );

  store.dispatch(loadingList());

  return runApp(App(
    store: store
  ));
}