import 'package:dio/dio.dart';
import 'package:photo_sharing_app/src/redux/actions/auth.dart';
import 'package:photo_sharing_app/src/redux/actions/load_user_list.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

import 'package:photo_sharing_app/src/models/user.dart';
import 'package:photo_sharing_app/src/models/app_state.dart';

class RegisterSuccess {
  final User detail;

  RegisterSuccess(this.detail);
}

Future<String> loadAsset() async {
  String raw = await rootBundle.loadString('assets/config.json');
  Map result = json.decode(raw);
  return result['uri'];
}

ThunkAction<AppState> register(String email, String password) {
  return (Store<AppState> store) async {
    store.dispatch(Loading());
    String uri = await loadAsset();
    Dio dio = Dio();
    await dio.post('$uri/user/register', data: {
      'email': email,
      'password': password
    })
      .then((res) => {
          store.dispatch(RegisterSuccess(User.fromJson(res.data))),
          store.dispatch(loadingList())
      });
  };
}