import 'package:dio/dio.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

import 'package:photo_sharing_app/src/models/user.dart';
import 'package:photo_sharing_app/src/models/app_state.dart';
import 'package:validators/validators.dart';

class Loading {}

class LoginSuccess {
  final User detail;

  LoginSuccess(this.detail);
}

class LoginFailed {}


Future<String> loadAsset() async {
  String raw = await rootBundle.loadString('assets/config.json');
  Map result = json.decode(raw);
  return result['uri'];
}

ThunkAction<AppState> login(String email, String password) {
  return (Store<AppState> store) async {
    store.dispatch(Loading());
    String uri = await loadAsset();
    Dio dio = Dio();
    Map data = isEmail(email) ? {
      'email': email
    } : {
      'uniqueHandle': email
    };
    await dio.post('$uri/user/login', data: data)
      .then((res) => {
        if (res.statusCode == 202 && res.data['password'] == password) {
          store.dispatch(LoginSuccess(User.fromJson(res.data)))
        } else {
          store.dispatch(LoginFailed())
        }
      })
      .catchError((onError) {
        store.dispatch(LoginFailed());
      });
  };
}