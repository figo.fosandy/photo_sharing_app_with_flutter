import 'package:dio/dio.dart';
import 'package:photo_sharing_app/src/redux/actions/edit_profile.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

import 'package:photo_sharing_app/src/models/app_state.dart';

class ImageLoading {}

class UploadImageSuccess {}

Future<String> loadAsset() async {
  String raw = await rootBundle.loadString('assets/config.json');
  Map result = json.decode(raw);
  return result['uri'];
}

ThunkAction<AppState> uploadImage(String email, Map payload, Map userPayload) {
  return (Store<AppState> store) async {
    store.dispatch(ImageLoading());
    String uri = await loadAsset();
    Dio dio = Dio();
    FormData formData = FormData.fromMap(Map<String, dynamic>.from(payload));
    await dio.post('$uri/user/image/$email', data: formData)
      .then((res) => {
          store.dispatch(UploadImageSuccess()),
          if (payload['isDp'] != null) {
            store.dispatch(editProfile(email, userPayload))
          }
      });
  };
}