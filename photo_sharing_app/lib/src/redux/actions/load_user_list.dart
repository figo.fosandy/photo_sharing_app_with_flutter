import 'package:dio/dio.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

import 'package:photo_sharing_app/src/redux/actions/auth.dart';
import 'package:photo_sharing_app/src/models/user.dart';
import 'package:photo_sharing_app/src/models/app_state.dart';

class LoadingListSuccess {
  final List<User> list;
  
  LoadingListSuccess(this.list);
}

Future<String> loadAsset() async {
  String raw = await rootBundle.loadString('assets/config.json');
  Map result = json.decode(raw);
  return result['uri'];
}

ThunkAction<AppState> loadingList() {
  return (Store<AppState> store) async {
    store.dispatch(Loading());
    String uri = await loadAsset();
    Dio dio = Dio();
    await dio.get('$uri/user/list')
      .then((res) => {
          store.dispatch(
            LoadingListSuccess(
              (res.data as List).map((user) => User.fromJson(user)).toList()  
            )
          )
        });
  };
}