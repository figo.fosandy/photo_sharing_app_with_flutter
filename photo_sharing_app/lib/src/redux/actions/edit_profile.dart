import 'package:dio/dio.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

import 'package:photo_sharing_app/src/redux/actions/auth.dart';
import 'package:photo_sharing_app/src/models/user.dart';
import 'package:photo_sharing_app/src/models/app_state.dart';

class EditProfileSuccess {
  final User detail;

  EditProfileSuccess(this.detail);
}

Future<String> loadAsset() async {
  String raw = await rootBundle.loadString('assets/config.json');
  Map result = json.decode(raw);
  return result['uri'];
}

ThunkAction<AppState> editProfile(String email, Map payload) {
  return (Store<AppState> store) async {
    store.dispatch(Loading());
    String uri = await loadAsset();
    Dio dio = Dio();
    await dio.put('$uri/user/edit/$email', data: payload)
      .then((res) => {
          store.dispatch(EditProfileSuccess(User.fromJson(res.data)))
      });
  };
}