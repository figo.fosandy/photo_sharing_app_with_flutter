import 'package:photo_sharing_app/src/models/app_state.dart';
import 'package:photo_sharing_app/src/redux/actions/auth.dart';
import 'package:photo_sharing_app/src/redux/actions/edit_profile.dart';
import 'package:photo_sharing_app/src/redux/actions/load_user_list.dart';
import 'package:photo_sharing_app/src/redux/actions/register.dart';
import 'package:photo_sharing_app/src/redux/actions/upload_image.dart';

import 'package:redux/redux.dart';

AppState appStateReducer(AppState state, action) {
  return loginReducer(state, action);
}

Reducer<AppState> loginReducer = combineReducers<AppState>([
  TypedReducer<AppState, Loading>(loadingReducer),
  TypedReducer<AppState, LoginSuccess>(loginSuccessReducer),
  TypedReducer<AppState, LoginFailed>(loginFailedReducer),
  TypedReducer<AppState, LoadingListSuccess>(loadingListSuccess),
  TypedReducer<AppState, RegisterSuccess>(registerSuccess),
  TypedReducer<AppState, EditProfileSuccess>(editProfileSuccess),
  TypedReducer<AppState, ImageLoading>(imageLoading),
  TypedReducer<AppState, UploadImageSuccess>(uploadImageSuccess)
]);

AppState loadingReducer(AppState state, Loading action) {
  return state.copyWith(loading: true);
}

AppState loginSuccessReducer(AppState state, LoginSuccess action) {
  return state.copyWith(loading: false, isLogin: true, validate: true, detail: action.detail);
}

AppState loginFailedReducer(AppState state, LoginFailed action) {
  return state.copyWith(loading: false, isLogin: false, validate: false);
}

AppState loadingListSuccess(AppState state, LoadingListSuccess action) {
  return state.copyWith(loading: false, list: action.list);
}

AppState registerSuccess(AppState state, RegisterSuccess action) {
  return state.copyWith(loading: false, detail: action.detail);
}

AppState editProfileSuccess(AppState state, EditProfileSuccess action) {
  return state.copyWith(loading: false, detail: action.detail);
}

AppState imageLoading(AppState state, ImageLoading action) {
  return state.copyWith(imageLoading: true);
}

AppState uploadImageSuccess(AppState state, UploadImageSuccess action) {
  return state.copyWith(imageLoading: false);
}