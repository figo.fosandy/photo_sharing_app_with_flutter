import 'package:flutter/material.dart';
import 'package:photo_sharing_app/src/models/user.dart';
import 'package:photo_sharing_app/src/redux/actions/register.dart';
import 'package:validators/validators.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'package:photo_sharing_app/src/models/app_state.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _rePasswordController = TextEditingController();
  bool _emailValidate = false;
  bool _passwordValidate = false;
  bool _rePasswordValidate = false;
  String _emailValidationError = 'Please fill the email';
  String _passwordValidationError = 'Please fill the password';
  String _rePasswordValidationError = 'Please reenter the password';

  void _emailValidation(String email, List<User> list) {
    if (email.isEmpty) {
      setState(() {
        _emailValidate = false;
        _emailValidationError = 'Please fill the email';
      });
    } else if (! isEmail(email)) {
      setState(() {
        _emailValidate = false;
        _emailValidationError = 'Invalid email';
      });
    } else if (! contains(email, 'realuniversity')) {
      setState(() {
        _emailValidate = false;
        _emailValidationError = 'Invalid domain';
      });
    } else if (list.any((user) => user.email == email)) {
      setState(() {
        _emailValidate = false;
        _emailValidationError = 'Email already taken';
      });
    } else {
      setState(() {
        _emailValidate = true;
        _emailValidationError = null;
      });
    }
  }

  void _passwordValidation(String password) {
    setState(() {
      _rePasswordValidate = password == _rePasswordController.text;
      _rePasswordValidationError = password == _rePasswordController.text ? null : 'Password missmatch';
    });
    if (password.isEmpty) {
      setState(() {
        _passwordValidate = false;
        _passwordValidationError = 'Please fill the password';
      });
    } else if (password.length < 6) {
      setState(() {
        _passwordValidate = false;
        _passwordValidationError = 'Password must be at least 6 characters';
      });
    } else {
      setState(() {
        _passwordValidate = true;
        _passwordValidationError = null;
      });
    }
  }

  void _rePasswordValidation(String rePassword) {
    if (rePassword.isEmpty) {
      setState(() {
        _rePasswordValidate = false;
        _rePasswordValidationError = 'Please reenter the password';
      });
    } else if (rePassword != _passwordController.text) {
      setState(() {
        _rePasswordValidate = false;
        _rePasswordValidationError = 'Password missmatch';
      });
    } else {
      setState(() {
        _rePasswordValidate = true;
        _rePasswordValidationError = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, _ViewModel>(
        converter: (Store<AppState> store) => _ViewModel.create(store),
        builder: (BuildContext context, _ViewModel viewModel) => _bodyRegisterPage(viewModel)
      )
    );
  }

  Widget _registerLoading(_ViewModel model) {
    AppState appState = model.appState;
    if(appState.detail.email != null && appState.detail.firstName == null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, '/profileCreation');
      });
    }
    return Text( appState.detail.email != null && appState.detail.firstName == null ? 'Register succesfull' : appState.loading ? 'Loading...' : '',
      textAlign: TextAlign.center,
      style: TextStyle(color: Colors.green)
    );
  }

  Widget _bodyRegisterPage(viewModel) {
    final _ViewModel model = viewModel; 
    return SafeArea(
      child: ListView(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        children: <Widget>[
          SizedBox(height: 15.0),
          Column(
            children: <Widget>[
              Image.asset('assets/logo.png'),
            ]
          ),
          SizedBox(height: 15.0),
          TextField(
            decoration: InputDecoration(
              filled: true,
              labelText: 'Email id',
              errorText: _emailValidationError,    
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.green),
              ),
              helperText: 'Email id is available',
              helperStyle: TextStyle(color: Colors.green)
            ),
            enabled: ! model.appState.loading && ! (model.appState.detail.email != null && model.appState.detail.firstName == null),
            controller: _emailController,
            onChanged: (text) => _emailValidation(text, model.appState.list),
          ),
          SizedBox(height: 10.0),
          TextField(
            decoration: InputDecoration(
              filled: true,
              labelText: 'Password',
              errorText: _passwordValidationError
            ),
            controller: _passwordController,
            obscureText: true,
            onChanged: _passwordValidation,
            enabled: ! model.appState.loading && ! (model.appState.detail.email != null && model.appState.detail.firstName == null),
          ),
          SizedBox(height: 10.0),
          TextField(
            decoration: InputDecoration(
              filled: true,
              labelText: 'Re-enter Password',
              errorText: _rePasswordValidationError
            ),
            controller: _rePasswordController,
            obscureText: true,
            onChanged: _rePasswordValidation,
            enabled: ! model.appState.loading && ! (model.appState.detail.email != null && model.appState.detail.firstName == null),
          ),
          ButtonBar(
            children: <Widget>[
              FlatButton(
                child: Text('Cancel'),
                onPressed: ! model.appState.loading && ! (model.appState.detail.email != null && model.appState.detail.firstName == null) ? () {
                  _emailController.clear();
                  _passwordController.clear();
                  _rePasswordController.clear();
                  setState(() {
                    _emailValidate = false;
                    _passwordValidate = false;
                    _rePasswordValidate = false;
                    _emailValidationError = 'Please fill the email';
                    _passwordValidationError = 'Please fill the password';
                    _rePasswordValidationError = 'Please reenter the password';
                  });
                } : null
              ),
              RaisedButton(
                child: Text('Register'),
                onPressed: _emailValidate && _passwordValidate && _rePasswordValidate && ! model.appState.loading && ! (model.appState.detail.email != null && model.appState.detail.firstName == null) ? () {
                  model.onRegister(_emailController.text, _passwordController.text);
                } : null
              )
            ]
          ),
          SizedBox(height: 5.0),
          _registerLoading(model),
          SizedBox(height: 5.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Already have an account? "),
              GestureDetector(
                onTap: () {
                    Navigator.pushReplacementNamed(context, '/login');
                },
                child: Text('Login',
                  style: TextStyle(fontWeight: FontWeight.bold)
                )
              )
            ],
          )
        ]
      )
    );
  }
}

class _ViewModel {
  final AppState appState;
  final Function(String, String) onRegister;

  _ViewModel({
    this.appState,
    this.onRegister,
  });

  factory _ViewModel.create(Store<AppState> store) {
    _onRegister(String email, String password) {
      store.dispatch(register(email, password));
    }

    return _ViewModel(
      appState: store.state,
      onRegister: _onRegister,
    );
  }
}