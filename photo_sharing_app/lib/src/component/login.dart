import 'package:flutter/material.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'package:photo_sharing_app/src/models/app_state.dart';
import 'package:photo_sharing_app/src/redux/actions/auth.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  bool _usernameValidate = true;
  bool _passwordValidate = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, _ViewModel>(
        converter: (Store<AppState> store) => _ViewModel.create(store),
        builder: (BuildContext context, _ViewModel viewModel) => _bodyLoginPage(viewModel)
      )
    );
  }

  Widget _loginLoading(_ViewModel model) {
    AppState appState = model.appState;
    if (appState.isLogin) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, '/home');
      });
    } 
    return Text( appState.loading ? 'Loading...' : appState.isLogin ? 'Login successful' : appState.validate ? '' : 'Invalid Username or Password',
      textAlign: TextAlign.center,
      style: TextStyle(color: appState.loading || appState.isLogin ? Colors.green : Colors.red)
    );
  }

  Widget _bodyLoginPage(viewModel) {
    final _ViewModel model = viewModel;
    return SafeArea(
      child: ListView(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        children: <Widget>[
          SizedBox(height: 80.0),
          Column(
            children: <Widget>[
              Image.asset('assets/logo.png'),
            ]
          ),
          SizedBox(height: 20.0),
          TextField(
            decoration: InputDecoration(
              filled: true,
              labelText: 'Email id or Unique Handle',
              errorText: ! _usernameValidate ? 'Please fill the email' : null
            ),
            controller: _usernameController,
            enabled: ! model.appState.loading,
            onChanged: (text) {
              if (text.isNotEmpty) {
                setState(() {
                  _usernameValidate = true;
                });
              }
            }
          ),
          SizedBox(height: 12.0),
          TextField(
            decoration: InputDecoration(
              filled: true,
              labelText: 'Password',
              errorText: ! _passwordValidate ? 'Please fill the password' : null
            ),
            controller: _passwordController,
            obscureText: true,
            enabled: ! model.appState.loading,
            onChanged: (text) {
              if (text.isNotEmpty) {
                setState(() {
                  _passwordValidate = true;
                });
              }
            }
          ),
          ButtonBar(
            children: <Widget>[
              FlatButton(
                child: Text('Cancel'),
                onPressed: model.appState.loading ? null : () {
                  _usernameController.clear();
                  _passwordController.clear();
                  setState(() {
                    _usernameValidate = true;
                    _passwordValidate = true;
                  });
                }
              ),
              RaisedButton(
                child: Text('Login'),
                onPressed: model.appState.loading ? null : () async {
                  if (_usernameController.text.isNotEmpty && _passwordController.text.isNotEmpty) {
                    await model.onLogin(_usernameController.text, _passwordController.text);
                  } else {
                    setState(() {
                      _usernameController.text.isEmpty ? _usernameValidate = false : _usernameValidate = true;
                      _passwordController.text.isEmpty ? _passwordValidate = false : _passwordValidate = true;
                    });
                  }
                }
              )
            ]
          ),
          _loginLoading(model),
          SizedBox(height: 30.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Don't have an account? "),
              GestureDetector(
                onTap: () {
                    Navigator.pushReplacementNamed(context, '/register');
                },
                child: Text('Register',
                  style: TextStyle(fontWeight: FontWeight.bold)
                )
              )
            ],
          )
        ]
      )
    );
  }
}

class _ViewModel {
  final AppState appState;
  final Function(String, String) onLogin;

  _ViewModel({
    this.appState,
    this.onLogin
  });

  factory _ViewModel.create(Store<AppState> store) {
    _onLogin(String email, String password) {
      store.dispatch(login(email, password));
    }

    return _ViewModel(
      appState: store.state,
      onLogin: _onLogin
    );
  }
}