import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:photo_sharing_app/src/models/user.dart';
import 'package:photo_sharing_app/src/redux/actions/edit_profile.dart';
import 'package:photo_sharing_app/src/redux/actions/upload_image.dart';
import 'package:validators/validators.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'package:photo_sharing_app/src/models/app_state.dart';

class ProfileCreationPage extends StatefulWidget {
  @override
  _ProfileCreationPageState createState() => _ProfileCreationPageState();
}

class _ProfileCreationPageState extends State<ProfileCreationPage> {
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _uniqueHandleController = TextEditingController();
  bool _firstNameValidate = false;
  bool _lastNameValidate = false;
  bool _uniqueHandleValidate = false;
  String _firstNameValidationError = 'Please fill the first name';
  String _lastNameValidationError = 'Please fill the last name';
  String _uniqueHandleValidationError = 'Please fill the unique handle';

  File _image;
  
  void _firstnameValidation(String firstName) {
    if (firstName.isEmpty) {
      setState(() {
        _firstNameValidate = false;
        _firstNameValidationError = 'Please fill the first name';
      });
    } else if (! RegExp(r'^[a-zA-Z]+(\s[a-zA-Z]+)*$').hasMatch(firstName)) {
      setState(() {
        _firstNameValidate = false;
        _firstNameValidationError = 'Invalid first name';
      });
    } else {
      setState(() {
        _firstNameValidate = true;
        _firstNameValidationError = null;
      });
    }
  }

  void _lastNameValidation(String lastName) {
    if (lastName.isEmpty) {
      setState(() {
        _lastNameValidate = false;
        _lastNameValidationError = 'Please fill the last name';
      });
    } else if (! RegExp(r'^[a-zA-Z]+(\s[a-zA-Z]+)*$').hasMatch(lastName)) {
      setState(() {
        _lastNameValidate = false;
        _lastNameValidationError = 'Invalid last name';
      });
    } else {
      setState(() {
        _lastNameValidate = true;
        _lastNameValidationError = null;
      });
    }
  }

  void _uniqueHandleValidation(String uniqueHandle, List<User> list) {
    if (uniqueHandle.isEmpty) {
      setState(() {
        _uniqueHandleValidate = false;
        _uniqueHandleValidationError = 'Please fill the unique handle';
      });
    } else if (! isAlphanumeric(uniqueHandle)) {
      setState(() {
        _uniqueHandleValidate = false;
        _uniqueHandleValidationError = 'Invalid unique handle';
      });
    } else if (list.any((user) => user.uniqueHandle == uniqueHandle)) {
      setState(() {
        _uniqueHandleValidate = false;
        _uniqueHandleValidationError = 'Unique handle already taken';
      });
    } else {
      setState(() {
        _uniqueHandleValidate = true;
        _uniqueHandleValidationError = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, _ViewModel>(
        converter: (Store<AppState> store) => _ViewModel.create(store),
        builder: (BuildContext context, _ViewModel viewModel) => _bodyProfileCreationPage(viewModel)
      )
    );
  }

  Widget _editProfileLoading(_ViewModel model) {
    AppState appState = model.appState;
    if(appState.detail.firstName != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, '/login');
      });
    }
    return Text( appState.detail.firstName != null 
                  ? 'Creating profile succesfull'
                  : _image != null
                    ? ( appState.imageLoading
                        ? 'Uploading image...'
                        : appState.loading ? 'Uploading image successful, Creating Profile...' : ''
                      )
                    : appState.loading ? 'Creating Profile ...' : '',
      textAlign: TextAlign.center,
      style: TextStyle(color:  Colors.green)
    );
  }  

  Widget get image {
    if (_image == null) {
      return Icon(Icons.add_a_photo, size: 200.0);
    }
    return Image.file(_image, fit: BoxFit.cover, height: 200.0);
  }

  Future chooseImage(source) async {
    var image = await ImagePicker.pickImage(source: source);
    setState(() {
      _image = image;
    });
  }

  Future getImage() async {
    return await showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text('Choose one of them below'),
          children: <Widget>[
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context);
                chooseImage(ImageSource.camera);
              },
              child: Text('Pick from camera')
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context);
                chooseImage(ImageSource.gallery);
              },
              child: Text('Pick from gallery')
            ),
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Cancel')
            )
          ]
        );
      }
    );
  }

  Widget _bodyProfileCreationPage(viewModel) {
    final _ViewModel model = viewModel; 
    return SafeArea(
      child: ListView(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        children: <Widget>[
          SizedBox(height: 15.0),
          GestureDetector(
            child: image,
            onTap: model.appState.detail.firstName == null && ! model.appState.loading && ! model.appState.imageLoading ? getImage : null
          ),
          SizedBox(height: 15.0),
          TextField(
            decoration: InputDecoration(
              filled: true,
              labelText: 'First Name',
              errorText: _firstNameValidationError
            ),
            enabled: model.appState.detail.firstName == null && ! model.appState.loading && ! model.appState.imageLoading,
            onChanged: _firstnameValidation,
            controller: _firstNameController
          ),
          SizedBox(height: 10.0),
          TextField(
            decoration: InputDecoration(
              filled: true,
              labelText: 'Last Name',
              errorText: _lastNameValidationError
            ),
            enabled: model.appState.detail.firstName == null && ! model.appState.loading && ! model.appState.imageLoading,
            onChanged: _lastNameValidation,
            controller: _lastNameController
          ),
          SizedBox(height: 10.0),
          TextField(
            decoration: InputDecoration(
              filled: true,
              labelText: 'Unique Handle',
              errorText: _uniqueHandleValidationError,    
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.green),
              ),
              helperText: 'Unique handle is available',
              helperStyle: TextStyle(color: Colors.green)
            ),
            enabled: model.appState.detail.firstName == null && ! model.appState.loading && ! model.appState.imageLoading,
            onChanged: (text) => _uniqueHandleValidation(text, model.appState.list),
            controller: _uniqueHandleController
          ),
          ButtonBar(
            children: <Widget>[
              FlatButton(
                child: Text('Cancel'),
                onPressed: model.appState.detail.firstName == null && ! model.appState.loading && ! model.appState.imageLoading ? () {
                  _firstNameController.clear();
                  _lastNameController.clear();
                  _uniqueHandleController.clear();
                  setState(() {
                    _firstNameValidate = false;
                    _lastNameValidate = false;
                    _uniqueHandleValidate = false;
                    _firstNameValidationError = 'Please fill the first name';
                    _lastNameValidationError = 'Please fill the last name';
                    _uniqueHandleValidationError = 'Please fill the unique handle';
                  });
                } : null
              ),
              RaisedButton(
                child: Text('Create'),
                onPressed: _firstNameValidate && _lastNameValidate && _uniqueHandleValidate && ! model.appState.loading && model.appState.detail.firstName == null && ! model.appState.imageLoading ? () async {
                  if (_image != null) {
                    int dateNow = DateTime.now().millisecondsSinceEpoch;
                    List fileDetail = _image.path.split('/').last.split('.');
                    String fileName = '${fileDetail.first}$dateNow.${fileDetail.last}';
                    model.onUploadImage(model.appState.detail.email, {
                      'name': fileName,
                      'image': await MultipartFile.fromFile(_image.path, filename: fileName),
                      'isDp': true
                    }, {
                      'firstName': _firstNameController.text,
                      'lastName': _lastNameController.text,
                      'uniqueHandle': _uniqueHandleController.text,
                      'displayPicture': fileName
                    });
                  } else {
                    model.onEditProfile(model.appState.detail.email, {
                      'firstName': _firstNameController.text,
                      'lastName': _lastNameController.text,
                      'uniqueHandle': _uniqueHandleController.text
                    });
                  }
                } : null
              )
            ]
          ),
          SizedBox(height: 10.0),
          _editProfileLoading(model)
        ]
      )
    );
  }
}

class _ViewModel {
  final AppState appState;
  final Function(String, Map) onEditProfile;
  final Function(String, Map, Map) onUploadImage;

  _ViewModel({
    this.appState,
    this.onEditProfile,
    this.onUploadImage
  });

  factory _ViewModel.create(Store<AppState> store) {
    _onEditProfile(String email, Map payload) {
      store.dispatch(editProfile(email, payload));
    }

    _onUploadImage(String email, Map payload, Map userPayload) {
      store.dispatch(uploadImage(email, payload, userPayload));
    }

    return _ViewModel(
      appState: store.state,
      onEditProfile: _onEditProfile,
      onUploadImage: _onUploadImage
    );
  }
}