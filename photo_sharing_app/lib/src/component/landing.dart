import 'package:flutter/material.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:redux/redux.dart';

import 'package:photo_sharing_app/src/models/app_state.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  void _onLanding(viewModel) {
    final _ViewModel model = viewModel;
    final AppState appState = model.appState;
    String route;
    if (appState.isLogin) {
      route = '/home';
    } else if(appState.detail.email != null && appState.detail.firstName == null) {
      route = '/profileCreation';
    } else {
      route = '/login';
    }
    Future.delayed(const Duration(seconds: 5), () {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, route);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, _ViewModel>(
        converter: (Store<AppState> store) => _ViewModel.create(store),
        builder: (BuildContext context, _ViewModel viewModel) => _bodyProfileCreationPage(viewModel),
        onInitialBuild: (_ViewModel viewModel) => _onLanding(viewModel),
      )
    );
  }

  Widget _bodyProfileCreationPage(viewModel) {
    return SafeArea(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('Please wait, the application is preparing'),
            Loading(indicator: BallPulseIndicator(), size: 100.0)
          ]
        ),
      )
    );
  }
}

class _ViewModel {
  final AppState appState;

  _ViewModel({
    this.appState,
  });

  factory _ViewModel.create(Store<AppState> store) {
    return _ViewModel(
      appState: store.state
    );
  }
}