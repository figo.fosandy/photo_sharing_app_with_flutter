class User {
  final String email;
  final String firstName;
  final String lastName;
  final String displayPicture;
  final String uniqueHandle;
  final List<String> friends;

  User({
    this.email,
    this.firstName,
    this.lastName,
    this.displayPicture,
    this.uniqueHandle,
    this.friends
  });

  User copyWith({
    String email,
    String firstName,
    String lastName,
    String displayPicture,
    String uniqueHandle,
    List<String> friends}) {
      return User(
        email: email ?? this.email,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        displayPicture: displayPicture ?? this.displayPicture,
        uniqueHandle: uniqueHandle?? this.uniqueHandle,
        friends: friends ?? this.friends
      );
  }
  
  User.fromJson(Map json)
    : email = json['email'],
      firstName = json['firstName'],
      lastName = json['lastName'],
      displayPicture = json['displayPicture'],
      uniqueHandle = json['uniqueHandle'],
      friends = json['friends'] == null ? [] : (json['friends'] as List).whereType<String>().toList();

  Map toJson() => {
    'email': email,
    'firstName': firstName,
    'lastName':lastName,
    'displayPicture':displayPicture,
    'uniqueHandle': uniqueHandle,
    'friends': friends
  };

  @override
    String toString() {
      return toJson().toString();
  }
}