import 'package:photo_sharing_app/src/models/user.dart';

class AppState {
  final bool isLogin;
  final bool validate;
  final bool loading;
  final bool imageLoading;
  final User detail;
  final List<User> list;

  AppState({
    this.isLogin,
    this.validate,
    this.loading,
    this.imageLoading,
    this.detail,
    this.list
  });

  AppState.initialState() :
    isLogin = false,
    validate = true,
    loading = false,
    imageLoading = false,
    detail = User(),
    list = List.unmodifiable(<User>[]);

  AppState copyWith({
    bool isLogin,
    bool validate,
    bool loading,
    bool imageLoading,
    User detail,
    List<User> list
  }) =>
    AppState(
      isLogin: isLogin ?? this.isLogin,
      validate: validate ?? this.validate,
      loading: loading ?? this.loading,
      imageLoading: imageLoading ?? this.imageLoading,
      detail: detail ?? this.detail,
      list: list ?? this.list
    );

  static AppState fromJSON(dynamic json) {
    if (json == null) {
      return AppState.initialState();
    }
    return AppState(
      isLogin: json['isLogin'] as bool,
      validate: json['validate'] as bool,
      loading: json['loading'] as bool,
      imageLoading: json['imageLoading'] as bool,
      detail: User.fromJson(json['detail']),
      list: (json['list'] as List).map((user) => User.fromJson(user)).toList()
    );
  }

  dynamic toJson() => {
    'isLogin': isLogin,
    'validate': validate,
    'loading': loading,
    'detail': detail,
    'list': list
  };
  
  @override
    String toString() {
      return toJson().toString();
  }
}