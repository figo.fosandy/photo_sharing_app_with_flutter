import 'package:flutter/material.dart';
import 'package:photo_sharing_app/src/component/landing.dart';

import 'package:photo_sharing_app/src/component/login.dart';
import 'package:photo_sharing_app/src/component/home.dart';
import 'package:photo_sharing_app/src/component/profile_creation.dart';
import 'package:photo_sharing_app/src/component/register.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'package:photo_sharing_app/src/models/app_state.dart';

class App extends StatelessWidget {
  final Store<AppState> store;

  const App({Key key, this.store}) : super(key: key);

  Widget _appChild () {
    return MaterialApp(
      title: 'Photo Sharing App',
      initialRoute: '/landing',
      theme: ThemeData.dark(),
      onGenerateRoute: _getRoute
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: _appChild()
    );
  }
}

Route<dynamic> _getRoute(RouteSettings settings) {
  switch (settings.name) {
    case '/landing':
      return MaterialPageRoute(builder: (_) => LandingPage());
    case '/login':
      return MaterialPageRoute(builder: (_) => LoginPage());
    case '/home':
      return MaterialPageRoute(builder: (_) => HomePage());
    case '/register':
      return MaterialPageRoute(builder: (_) => RegisterPage());
    case '/profileCreation':
      return MaterialPageRoute(builder: (_) => ProfileCreationPage());
    default:
      return null;
  }
}