# Photo Sharing App With Flutter
Holiday Excercise is creating Photo Sharing App using Flutter and should be :
- Register (done), Login (done), Logout (in-progress)
- Create (done) or Edit Profile (in-progress)
- View User's Feeds (in-progress)
- Search, Follow & View (feeds) Friends (in-progress)


## Approach
I'm using hapi for rest api server, flutter_redux and redux 
for state management, loading for show loading indicator, 
dio for api call, validators to validate, redux_thunk middlewares 
for async redux actions, and redux_logging middlewares for 
redux state and actions logging, redux_persist middleware for 
persist the state when app closed.

## Step to run the code
First, download or clone this code
### Preparation
For rest api server is located on ff-api-photo-share directory, 
there is config.json so u can change uri of mongodb server and 
rest api server's port, and there is images directory for user's
images upload directory, u can empty out images directory for 
the begining. After that u can install package needed by this command
```sh
$ cd ff-api-photo-share
$ npm i
```
For the photo sharing app is located on photo\_sharing\_app directory, 
there is assets directory contains config.json so u can change uri of 
rest api server, and logo.png so u can't change the logo by replace 
logo.png with the logo you want
### Starting
Create two console or terminal. 
First: 
```sh
$ cd ff-api-photo-share
$ npm start
``` 
Second: 
```sh
$ cd photo_sharing_app
$ flutter run

```

## Problems
In Progress

## Notes
- Each you open the app you will go to the landing page, and wait until app done prepare
- If you haven't login, you will be redirect to login page
- If you have register but not create profile yet, you will be redirect to profile creation page
- If you have login before, you will be redirect to home/feed page
- You can login using e-mail id or unique handle
- If you doesn't have an account you can tap on **Register** on login page, after that you will be redirect to register page
- On register page, you must fill the field with valid values, so register button will be active
- On register page, validation for email id field is must be email with domain 'realuniversity' and available
- On register page, validation for password field is must be at least 6 characters and must be match with re-enter password field
- On register page, if you press on register, registration will run, and if register successful, it will be redirect to profile creation page
- On profile creation page, you must fill the field with valid values, so create button will be active
- On profile creation page, you can upload display picture (optional) if you want with tap the camera icon, and you can upload from camera or galery
- On profile creation page, validation for first name and last name field is must be alphabetical
- On profile creation page, validation for unique handle field is must be alphanumeric
- On profile creation page, if you press on create, profile creation will run, and if it successful, it will be redirect to login page